<!--
Issue title: Tracking issue for minor release: [e.g. 24.9.1]

Note that some of the steps are not yet updated for the change in
process required for the new release scheme.
-->

Info for the release
--------------------
- Series: <!--e.g. 24.9.x -->
- Version: <!-- e.g. 24.9.1 -->
- Release coordinator: <!-- name and GitLab handle -->
- Schedules:
  - Freeze date, first RC: <!-- date -->
  - Release date: <!-- date, 1-2 weeks from the first RC -->

Release status
--------------

<!--
Any news pertaining the release. Current blocker, image build
status etc.
-->

Tasks for release candidate(s)
------------------------------
- [ ] Smoke tests (boot, WiFi, mobile data, phone, SMS)
  - [ ] Halium 7 (Sony Xperia X)
  - [ ] Halium 9 (Volla Phone)
  - [ ] Halium 10 (???)
  - [ ] Halium 11 (Fairphone 4)
  - [ ] Halium 12 (Volla Phone X23)
- [ ] Promote images from `/daily`/`/devel` to `/rc` (pull-and-promote-rc.sh)
- [ ] Call for testing forum post: <!-- URL -->
- [ ] Call for testing announced on Telegram QA group.
- [ ] Call for testing blog post: <!-- URL -->

Tasks for actual release
------------------------
- [ ] Verify image has been sitting in the RC channel for at least 1 week.
- [ ] Verify image has no blocking issue.
- [ ] Promote images from `/rc` to `/stable`.
- [ ] Set up phaser in Ansible Playbook.
- [ ] Release announcement blog post: <!-- URL -->
- [ ] After ~5 days, ensure that phased percentage reaches 100%
