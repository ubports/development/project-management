UBports DevSync Notes 2023-06-22

What you have been working on? What can be unblocked for you?

Ratchanan:
	* Update usb-moded to properly tethering on FP4
	* ayatana-indicator-power: change the way it communicates the battery level to the UI
		* qmenumodel update (x-ayatana-progress vs. x-ayatana-level)
		* lomiri system settings UI update required, as well. (Stop querying the power indicator for the battery level)
		* lomiri UI update (indicator rendering)
	* hotspot problem for Volla * devices (tidy up Katherine's patch)
		* a different concept proposal sent upstream (NetworkManager) which met agreement by upstream
		* test on real hardware
	* Fix VP22 installer config (bootstrap)
Lionel:
	* Fix small clipboard bug when copying video URLs (in youtube-webapp)
	* LUITK not building in cross builder, debugging on this (recommendation by Guido: wipe LXD container and start from scratch)
Guido:
	* Fixes API docs
		* Fixed missing module level-pages, incorrect markup leading to broken links
		* Fixed incorrect relative links in search results
		* The last big hurdle: u1db-qt and QtFeedback (Qt 5.11 introduced libclang based qdoc comment parsing which needs integration with thebuild system and some deprecated markup syntax has been removed)
		* Meta-bug: https://gitlab.com/ubports/docs/api-docs/-/issues/1
	* we want regular rebuilds of API Docs in CI (probably weekly)
	* need to serve the documentation directly under api-docs.ubports.com rather than the current redirect, sooner than later before people bookmark and share deep links
Mike:
    


