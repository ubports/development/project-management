UBports DevSync Notes 2023-10-26

What you have been working on? What can be unblocked for you?

Alfred:
    - Qt6 experiments in a click package, advantage focal-support, can start now
    - libhybris-support not sufficient
    - packaging Tide IDE
    - Qt6Wayland libhybris integration insufficient for general app support
    
Jami
- VollaOS UT multiboot on Volla Phone 22 & X23

Ratchanan
- CI out of diskspace resolved
- qtwebengine build on CI ongoing
- qtwebengine crash: concurrency issue in Alfred's patch solved, fixed calling callbacks in correct threads, no more crashes during stress test with debug build
- upower: discussion with upstream on fake battery devices

Marius
- start work on port of Fairphone 5, splash shows, but no shell, adb, Recovery works 
- need help with shell from Nikita

General Topics

- Guido: CI running out of diskspace
	- Marius: separate repro from Jenkins CI server, purge aptly regularly
- Ratchanan: planning and contracts
	- Marius: sponsor side done, try to get it done this or next week
	- Marius: address consumer-grade readiness, foster community involvement
