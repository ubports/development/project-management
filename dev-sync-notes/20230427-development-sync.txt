UBports DevSync Notes 2023-04-27

What you have been working on? What can be unblocked for you?

Marius:
	* Continue quest on VoLTE, scripts are about to be shared in the near future
	* helping Rudra with making ISO for Ubuntu Luna
	* improve desktop experience of Lomiri
		* will create PRs on UBports GitLab, so we can cherry-pick changes to Debian
		* probably today


Jami:
	* OnePlus 5 fix blackscreen issue of Xwayland Libertine apps (by using SharedMemory backend)
		* MR pending (https://gitlab.com/ubports/porting/community-ports/android9/oneplus-5/android_device_oneplus_msm8998-common/-/merge_requests/2) until dependent xwayland-hybris change merged
	* Volla Phone X23
		* flash light toggle broken, writing 0|1 to sysfs node is insufficient, some ioctls need to be used instead.
			* Blocked by power-indicator/hfd-service, for the Volla Phone X23 we need libhybris based flashlight toggling
			* (Marius will provide his local copy of hfd-service providing flashlight backend support)
		* Likely next looking into UBports recovery for Halium 12
	* Looked into missing SIM unlock dialog. It shows after the wizzard has finished, but not when booting the device normally.
		* After wizard lomiri-indicator-network called from https://gitlab.com/ubports/development/core/lomiri/-/blob/f7086cc3/qml/Shell.qml#L734, will look into how the special "notification" on boot is supposed to work

Ratchanan:
	* Looking into device behaviour for access point mode (regarding password on Volla phone and on Rasberry Pi)
	* Dive into how WiFi actually work, review Katherine's patch regarding <algo> disabling support in NetworkManager
	* Making lomiri-indicator-network shows a notification 
	* Investigate on Volla 22 offline charge mode. 
		* <details on error analysis>
	* Look into build tooling etc. and into the implementation of the new release model


