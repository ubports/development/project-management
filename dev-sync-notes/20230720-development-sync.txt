# UBports DevSync Notes 2023-07-20

What you have been working on? What can be unblocked for you?

Ratchanan:
    - changelog/release notes for OTA-2
    - fixed installer config for Vollaphone 22
    - lomiri-thumbnailer: use fd-passing instead of racy credentails checking
    - content-hub: same issue, not fixed yet due to complexity involved, on hold
    - ofono APN database from LineageOS packaged, APN database not shipped in halium any more, should address issue with MMS sending
    
Jami
- Volla Phone X23: working USB finally including ADB & MTP, improvements currently device-specific but should be extended UT-wide so other devices (including Volla Phone 22 for example) could uses it as well instead of the old mtp-server
    - https://gitlab.com/ubports/development/core/packaging/umtp-responder repo needs to be created and packaging pushed
- work on missing wired headset support, add kernel patch from VP22 to fix jack device events, pulseaudio-droid support not enough yet
- 

Lionel:
	Worked on FingerPrint in lss, moved to "Lock & security" Page -> https://gitlab.com/ubports/development/core/lomiri-system-settings-security-privacy/-/merge_requests/16
	Worked on app adaptation for QT Auto scale ( Teleport, Dekko and Music App )
	
## Other topics

### hfd-service

- system level daemon which needs per-user vibration settings
    - replaces usensord which ran in the user session
    - options to store settings proposed by Ratchanan:
        - use AccountsService via DBus
        - implement our own storage backend and DBus-Interface
    - Guido: we already store similar settings in AccountsService and using it obviates the need for writing storage backend code
    - need a way to identify applications to apply specific settings, e.g. incoming call, message, keyboard, notification
	    - usensord used hardcoded binary names to allow/deny
          - Ratchanan: hardcoded binaries are ugly, hard to maintain, we could use a separate, privileged DBus API where access is granted to respective applications via AppArmor
