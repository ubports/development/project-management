UBports DevSync Notes 2023-11-23

What you have been working on? What can be unblocked for you?

Mike:
	* busy with other customer, no UBports related work
	* report about latest Ayatana Indicators upstream development progresses (esp. a11y and display indicator)

Ratchanan:
	* work on ayatana-indicator-power bump, finalize upower vendoring
	* fix lomiri-system-settings, so it does not depend on a-i-power anymore
	* import new version of a-i-power
	* discuss with Robert Tari about notification hints (see https://gitlab.com/ubports/development/core/content-hub/-/merge_requests/32)
	* waydroid shutdown, trigger camera HAL crash, so the camera app gets functional a few seconds after the waydroid shutdown

Marius:
	* FP5 & 5G support / porting.
	* Infrastructure work for 24.4.
	* Community work.
	* Discuss using the release concept.
		* We agreed to do the "branch off" after OTA-4 release (planned in January, somewhere...)
		* When branching off, no breaking changes should land in ubports/focal anymore


