﻿Development sync 12 April 2024  

*   What you've been doing? What can we unblock for you?  
    

Ratchanan

*   Review MRs
*   Fix build-tools to post comment for "UNSTABLE" build status
*   Continue bootstrapping devel-noble archive  
    

  

electrode (Alexander Richards)  

*   A-GPS experiment on Volla Phone w/ SUPL proxy
    *   Can't get it to talk to SUPL  
        
*   Get in touch with galmon.eu for GNSS/GPS almanac info part of SUPL
*   May attend less in the next few weeks due to studying  
    

Nikita  

*   VoLTE on Volla Phones / MTK devices
    *   [https://gitlab.com/NotKit/ofono-binder-plugin-ext-mtk](https://gitlab.com/NotKit/ofono-binder-plugin-ext-mtk)
        *   Got incoming calls signaling and answering the call to work  
            
        *   Needs few changes to ofono-binder-plugin to implement call list merging between regular/IMS calls  
            
*   Helping Muhammad with getting 20.04 on Fairphone 2  
    

Jami

*      Upgrade failure on Volla Phone 23 -> shutdowns on display off due to incomplete new firmware flash  
    *   Caused by too little space in the factory image's rootfs partition.  
        
    *   Maybe also reduce EXT4's reserved-space (for /root)?
        *   Ratchanan: may affect delta upgrade???  
            

Lionel

*   CalDAV synchronization adventure / the new address book stack  
    *   Some packaging issues.
    *   How can get all codes tested?
        *   Ratchanan: it's possible to let ubports-qa pull in all MR dependencies  
            
        *   Mike: it's also possible to test this in Debian unstable
        *   Ratchanan: MRs can target Focal as long as they're draft.

Mike

*   Packaging Teleports to Debian
*   Review MRs
*   Discuss with Lionel about the new address book stack
*   Will go on vacation for the next week.  
    

Alfred

*   Experiment with (WPE)WebKit on UT - put in Click package
    *   Contains QML binding upstream, although a bit bare bone
    *   A fork contains QML binding on Qt 6

Raoul

*   Get in touch with galmon.eu for GNSS/GPS almanac info part of SUPL