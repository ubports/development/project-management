UBports DevSync Notes 2024-11-08
What you have been working on? What can be unblocked for you?

20.04 OTA-6
	* Released! Freeze lifted tomorrow.

Ratchanan
	* Co-ordinate 20.04 OTA-6 testing
	* Revert VoLTE rollout.
		* Nikita: we still have to handle APN-related stuffs anyway.
		* Ratchanan: there are 4 things that we would have to work.
			* Update APN database.
			* Update APN provisioning plugin to accept IPv6 for VoLTE context
			* Update UI to allow VoLTE APN change.
			* Set accept_ra_defrtr

Nikita
	* Volla Phone Quintus
		* Audio in video recording
	* mir-android2-platform syncfd mainline ioctl support

Jami
	* Volla Phone Quintus
		* Bootloader
	* Volla Tablet

Guido
	* lomiri-push-service: fix unit tests
	* lomiri-ui-toolkit: possible fix for bottom margin Window.window.visibility (needs testing)
	* qtorganizer-eds: debug recurrence-test failures on Debian unstable
	* qmf: investigate history of dekko fork, package latest upstream for Debian
	* qwebdavlib: package for Debian

Mike
	* Upload QtContacts-sqlite related packages to Debian (addressbook app stack)
	* Coordinate Dekko porting qbs -> CMake
	* Teleports (holding back due to unit test failures during sbuild only Mike's system, not so on Guido's or Cosima's)
	* Coordinate Lomiri-related aspects of signond transition from Qt5 -> Qt6 in Debian
		* Initial idea to make dual-build for client libraries - maintainer not sure if that's possible.
		* Might force us to package signond-qt5 as src:pkg
	* lomiri-ui-toolkit build fixed in Debian, 22 src:pkgs migrated to testing as consequence today
	* lomiri-content-hub autopkgtests failing in 2.0.0-3, fixing it is W-I-P (also: drop dbus-x11 / dbus-launch usage in test scripts)
	* Next: Mir 2.14 failure due to LTO

Alfred
	* Fuax-DMABUF libgbm backend for libhybris
		* Need a shim library for libgbm, libdrm and more.
		* Could become a mess.
		* Probably not worth it.

Raoul
	* Testing Dekko snap
	* Testing daily-driving FP5

Lionel:
    - Fixing stuffs in QtPIM for ics calendar import. Tried to sync with Debian, but weird i had to patch some tests.
    - Fixed issue with abook2calsync not releasing qtcontact-sqlte database
    - Now trying to fix the issue of some ics properties not parsed in QtPIM QVersitOrganizer. If successfull, we will not need anymore ICal.js in calendar-app and ease the migration script from EDS to mkCal)
    
