UBports DevSync Notes 2023-04-20

What you have been working on? What can be unblocked for you?

Announcements:
	* OTA-3 softest soft-freeze starts today (. Hard freeze starts on Thursday next week (2023-09-28)

Marius:
	* organizational stuff, contract work, etc.
	* work on roadmap
	* step up as community manager
	* make contributors more visible
	* work on new ofono-binder plugin
	* libmce ......upower.... stub (@mariogrip: please complete)
	* unblock: merge a-i-power PR 

Ratchanan:
	* Reduce vendor specific code from lomiri-indicator-network
	* Code reviews
	* Removal of Peekier, not so easy as thought: we need a search engine provider fallback, so that people who have Peekier configured, end up with our hard-code default (DuckDuckGo)
	* Revisit releasing / development scheme (meeting scheduled to continue work on the concept document)
	* unblock: content-hub review, review other MRs, please
	* pick a freeze date? -> Mike: what's the status of the Pinephone rotation MRs? They should get into OTA-3.
	* come up with a process of how to assign issues / MRs to milestones / epics (MG, RS -> release scheme concept document)

Nikita:
	* continuing with HWC3 support - attempting to boot Pixel 7
	* looking into the changes needed to support multi-screen devices (like Galaxy Z Fold)
		* bring back tertiary display code from mir-android-platform/CAF?
	* debugging Volla X23 fingerprint sensor
	* Nikita demonstrated a foldable device with two displays (one tablet display, foldable, one front disable), Samsung Galaxy Z-Fold 3

Jami:
	* Fixing VNC Server input on 20.04 [regression from 16.04, resolved with CAVEAT]

Guido:
	* no active work on UT, waiting for supplier agreements...

Mike:
	* no active work this week


