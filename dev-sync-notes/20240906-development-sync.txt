UBports DevSync Notes 2024-09-06
What you have been working on? What can be unblocked for you?

Nikita
	* VoLTE switch, logo for VoLTE
		* VoLTE requires MediaTek/Android 12.

Jami
	* Volla Quintus issues
		* Wi-Fi drivers causes kernel panic

Azkali
	* Fixing up Noble PDK
	* Mir 2.x
	* Request review on MRs.
		* Aethercast
			* Blocked by wds - Ratchanan will have to have a deeper look.

Lionel
	* Digging around calendar backend switch.
	* Fix calendar on 24.04.
	* Test LOA.

Muhammad
	* Album art in media-hub
	* SW-implemented VoLTE
	* Ask for FP2 addition to system-image merge
	* Ask for advice on charging_only_adb usb mode

Alfred
	* Snapd enablement work
	* Pre-alpha release of Mimi browsers (WPEWebKit-based).

Ratchanan
	* Continue working on PulseAudio patches
		* Will let use have voice call, both with earpiece/speaker and with Bluetooth headset.
	* Make devel-noble images available on all devices currently on 20.04/devel

