Development sync for November 08, 2022, 1430 UTC


Agenda

    * What have you been working on this week? 
        * Save suggestions until after the meeting is over, please
    * Are you having trouble with anything?

    * Discuss points in backlog... should we put more stuff on the board?

    * When's our next sync?

Minutes

Mike:
    lomiri src:pkg upload to Debian experimental
    Lomiri terminal app: provide standalone qmltermwidget package in Debian unstable (still in NEW)
    Hosted weblate needs configuration/setup
    Blockers: What other packages do I need in Debian to get things up and running?
    
    
Jami:
	Volla Phone 22
		- https://devices.ubuntu-touch.io/device/mimameid is live (xenial), focal status support coming later.
		- lomiri-system-settings sometimes shows multiple lines for battery percentage graph as reported by Dr. Wurzer, reproduction means unknown still, will make an issue about this
		- Got running on mainline kernel (v6.1-rc3)! only UART serial console, simple framebuffer and volume button works for now, looks like a promising platform with hardware blocks similar to already mainlined and well working Chromebook MediaTek SoCs
			- Mainline kernel fork with added support: https://gitlab.com/mtk-mainline/mt6768/linux
	- Debian unstable
		- Will look into filing Lomiri packaging bugs
			- Specifically I found lomiri-ui-toolkit-examples should depend on qml-module-lomiri-layouts as calculator.qml requires it to run
		- lomiri-desktop-session should get a wayland-session .desktop file so it can be used to launch from a display manager, perhaps scripts (i.e. dm-lomiri-session) need a bit of adaptation so they doesn't have a hard-dependency on systemd (would also be used on Void Linux with runit or postmarketOS with OpenRC for example)
	UT 20.04
		- No progress on debugging ayatana battery/power indicator flashlight toggle not working, glib issues, will message Mike about this and other relevant Ayatana issues with priority order


Ratchanan:
    Focal rootfs lomiri dependency de pendent on lomiri wallpaper
    Importing Click in rootfs >> before publishing in open-store.io
    Ringtone on phone gsetting have write/migrate to lomiri name space
    Sms notifications broken due migration, we missed systemd in the package it misses the service to start
     Fix USSD. Qt MetaObject method lookup done with completely wrong signature.
     Fixed mms in Focal, apparmour issue.
	     With apparmour change mms should work as well as on Xenial
     Proximity sensor is only thing not working properly, has to be reworked this week
     Cellular package will be finalised.
     
    
Serge:
    Mediaplayer application rewrite the application was longtime out of support,  file types, css ect 
    Which app should be next? >>> Filemanager app << needs to switch to Qt >>> + Gallery app
    
    
Guido:
    
    - systematically identify and add ubports/xenial branches to package projects without one, making main or ubports/latest the default
- lomiri-system-settings-security-privacy: fix security-type setting not being updated, replace libaccountsservice API with raw DBus calls
- lomiri-system-settings: hide devmode in about plugin if no password/passphrase set
- lomiri-push-service: add test suite for rewritten exec-tool, debug and fix numerous tests failing after the unprivileged helper lookup redesign
- qmltermwidget: import Debian package, adjust lomiri-terminal-app
- lomiri-docviewer-app: work on integrating libreofficekit build into click package build process
- reviews
- Ratchanan is going to check truststore issue
- OTA 24 is blocking
    
    
For in the Newsletter

Xenial end of feature exepting [EOL] Will go in maintainace mode
