Development sync for March 22, 2022, 1430 UTC


Agenda

    * What have you been working on this week? 
        * Save suggestions until after the meeting is over, please
    * Are you having trouble with anything?

    * Discuss points in backlog... should we put more stuff on the board?

    * When's our next sync?

Minutes


Ratchanan
	* remove USB thetering removed replaced by USB moded
	* ADB for PDK updated
	* review merge request


Needed:
Would great if Marius would have a look at Telephony ofono




Robert
	* qtpim: Cherry-pick patches from the dev branch for the kde/5.15 branch, revert patches from the kde/5.15 branch to make it build on 5.12   (tests needs to be looked at by someone)
	* lomiri, PDK: code review
	* lomiri-system-settings: Fix deprecated symbols
	* indicator-location: Migrate to lomiri-url-dispatcher
	* lomiri-indicator-network: Fix build failure, fix build warnings  (merge request > Ratchanan review) test needs to be looked at (for Debian)
	* Filed some bugs




Robert leaves this to someone (Marius?): Qt + Qmake
1. https://invent.kde.org/tari/qtpim/-/merge_requests should end up in https://invent.kde.org/qt/qt/qtpim/-/tree/kde/5.15

2. The following will make it build on Focal Qt 5.12 :

	* git revert 2a8a9374ad977e9fd9f79645e626fcee5e4aa750 2020 11 02 12:46:27 Fix calendardemo example
	* git revert 4a3b6e77d39c5fe06d889cecd150a67476a2d592 2020 10 01 17:25:04 More QDateTime(QDate) -> QDate::startOfDay() fixes
	* git revert da8da06580aa3f8bb36eed86d744b2f1ea6b16e3 2020 10 01 17:08:38 Remove usage of deprecated API from the declarative
	* git revert 923aca0d2169060773dcaa3b1440154a0751f88b 2020 10 01 16:16:49 Remove usage of deprecated QtAlgorithms
	* git revert 925d6de2f1bc86023a378b3f5955122c6e04e9ae 2020 10 01 16:16:22 Fix QList-from-QSet conversions
	* git revert a0ddf8d4f3604190946f9d8b59aafb2943487e1d 2020 10 01 16:05:18 Use QRegularExpression instead of the deprecated QRegExp
	* git revert f9a8f0fc914c040d48bbd0ef52d7a68eea175a98 2020 10 01 13:14:25 Remove use of deprecated QString methods
	* git revert cfdeb1637b96dcfb729c00a10430b03c293abc77 2020 08 17 13:35:54 Prefer QDate::startOfDay() over QDateTime(const QDate &)
	* git revert 6675d412a2f2ae2df7bb626624d6930c784d9189 2020 02 27 09:59:21 Deprecation fix: conversions between QList and QSet
	* git revert 848d4a2b49adca12666081788a05a8fc5d9fbb56 2020 02 25 15:15:12 Use Qt::SplitBehavior in preference to QString::SplitBehavior

3. Fix tests on both Debian Unstable and Focal


------

Porting infra needs a revamp

Every phone is a fork

New porting method <<< use image pinephone and fork that because not so much android.

Good to have Mediatek dominace Qalcomm (Google 3A) device Mediatek (VollaX) device as developer device
