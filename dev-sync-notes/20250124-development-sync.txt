UBports DevSync Notes 2025-01-24
What you have been working on? What can be unblocked for you?

Ratchanan
	* Looking into still missing packages from the seed
		* qtlomiri-appmenutheme
		* signon-apparmor-extension
		* Still missing: account-polld-plugins-go
			* Lionel started the work almost a year ago, but hasn't finished it.
			* Mike might try to squeeze this into paid work for Guido.
	* Work on Libertine support
		* Has to change now container is set up.
		* Now works.
		* Some rendering issues with GTK. But we'll have to leave it like that for now.

Jami
	* Volla Tablet
		* Fight with bootloader (and recover from brick), add misssing bootloader functionality required for shipping
	* umtp-responder
		* apparmor-easyprof-ubuntu for XDG_*_DIR: https://gitlab.com/ubports/development/core/apparmor-easyprof-ubuntu/-/merge_requests/41
	* FIx broken device port pipelines: https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools/-/commit/45bd65e9817293c5b86261d3f0bc0cdb61288ea3

Azkali
	* Volla Phone X23
		* Support new display. Adapting BSP
	* QtMir
		* Orientation breakage
	* USB is broken on (?)
		* FTrace in kernel is broken on some kernel (?), making it harder to debug

Nikita
	* IMS APN in oFono
	* Volla Quintus
		* Fingerprint - compare with Android
	* Side project: Ubuntu Touch as Android's Dynamic System Updates
		* https://github.com/ubuntu-touch-unihertz-titan/ubuntu-touch-experimental-gsi

Alfred
	* LUITK-core24 content snap
	* Ships close to all of apt's QML Modules 
	* Uses hybris-2404 on Touch, mesa-2404 on Ubuntu Desktop

Mike, Guido
	* Dekko & QMF on desktop
		* Can handle large inbox now.
		* QMF is now co-maintained by Guido, 2 developers (?) from SailfishOS
		* Possibility to include QMF in rootfs?
			* Ratchanan: doubt
	* Fix Mir build in Debian on arm{hf,64,el}
		* Caused by broken binutils affecting LTO
	* Planning for upcoming Debian Trixie freeze
	* Teleports: downgrading tdlib to 'td1.8.11' (uploaded a separate src:pkg alongside 'td')
	* Ask Cosima to work on UBSync
	* lomiri-greeter: look for DE icons in Ayatana-greeter-badges

Neochapay
	* LSS security privacy passcode problem
		* Polkit API change
		* Mike: maybe related to .pkla being deprecated.
		* Ratchanan: will try install polkitd-pkla and see if it fixes anything
