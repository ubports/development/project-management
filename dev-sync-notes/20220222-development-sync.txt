Development sync for February 22, 2022, 1430 UTC

unamed person that delated these notes please be respectfull and not delete stuff of others. When you have a good reason to do this please expose yourself and explain why this should be deleted.


Agenda

	* What have you been working on this week? 
		* Save suggestions until after the meeting is over, please
	* Are you having trouble with anything?

	* Discuss roadmap what are the scenario's for the coming months this year

	* When's our next sync?

Minutes

Guido
	* draft on overlay files documentation: https://gitlab.com/-/snippets/2257583#overlay-method-for-gitlab-ci-script-based-builds
		* (florian should merge docs.ubport.com repo to gitlab)
		* should be add to current porting (device specific) only for focal
	* Lomiri - upstart removal
	* history service, address-book-service, buteo-syncfw: DBus activation
	* renaming telephony-service
	* Ubuntu-touch-settings: gsettings vendoring
	* porting nuntium to focal
	* fork telephaty ofono? Discuss with maintainers (Ratchanan will check with plasma qt telegram group)
	* migration, Ratchanan needs to make guido maintainer
Robert
	* keyboard indicator, layouts + AccountsService
	* libayatona-common compilation flags fix
	* Power indicator - build with Lomiri features
	* lomiri-schemas  - fixed symlink path
	* lomiri-sounds - change sound directory to base directory
	* Sound indicator - build with -Werror + compilation cleanup
 Mike
	* release lomiri-schemas 0.1.1, lomiri-sounds 22.02
	* Ayatana Indicators release series 22.02.x >>> could be pulled in to focal
	* PR reviews for Robert Tari (Ayatana Indicators)
	* Fix qdjango via NMU in Debian testing/unstable
	* click autopkgtest failures (when run as root)
Ratchanan
	* Fix usbmoded, ADB works now >>> Mike review
	* UI system settings not hooked up, wil do next
	* crossbuilder: bugfix for Qt
	* debian packaging
	* review telephony package MRs
	* Needs clarification status from Marius on locationd and platform API
		* https://github.com/ubports/locationd/commit/00731cf51966279264d0fc58a0d89fe0ee7aebc2   should be split in different commits and needs rewriting <<< Mike will chase
Alberto
	* mediahub related components: lomiri-system-setings sound panel now works
	* sytem-settings unconfined, apparmor rules should be changed
	* DBus names should be changed, merge request
	* made ubuntu touch cli installer and than could work with GUI, has device specific settings https://gitlab.com/mardy/ubports-flasher
	* try to merge contact backend xenial
		* Florian should help mardy with migrating docker stuff >> documentation
Raoul
Alexander

april
1 contracter dev + volunteers
contracters in speare
Lomiri touch in deb 12

Ratchenan Lomiri dev (Dalton)
Marius knows all

How much dependent on Ubuntu
All unstable debian, with all packages upstream >>> independent from ubuntu

Seperation between downstream upstream this makes decoupeling

after xenial Ubuntu dropped phone part 

Mike >>> Debian formalistic effort, that they are in good quality state

>>> 

another half year contracting

To quality settle in we need a lot of power users to test <<<
Porter docs up to date >>>

Porters

testers (flash)

Speed up dev 

Extra dev for 2 days or + 



