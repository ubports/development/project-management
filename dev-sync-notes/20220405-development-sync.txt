Development sync for March 22, 2022, 1430 UTC


Agenda

    * What have you been working on this week? 
        * Save suggestions until after the meeting is over, please
    * Are you having trouble with anything?

    * Discuss points in backlog... should we put more stuff on the board?

    * When's our next sync?

Minutes


Terminal (clicable) in focal?


Guido
	* systematically identified projects which are both on GitLab and not archived on Github: https://gitlab.com/ubports/core/focal-projectmanagement-missing-packages/-/issues?sort=created_date&state=opened&author_username=gberh
	* looked into patching adbd to run unprivileged and run shells as the "phablet" user
	* PDK: implemented hooks when building images, used for dconf custom default values


Mike
	* Review Robert's work on qtpim kde/5.15 branch update
	* Upload to Debian unstable: wlcs 1.4.0-1
	* Upload to Debian unstable: libqofono 0.111-1
	* Open https://gitlab.com/ubports/infrastructure/devices.ubuntu-touch.io/-/issues/58
Ratchanan
	* SensorFw Debian packaging
	* Upgrade libgbinder
	* Plays with Redmi 9A
Nikita
	* Volla 22 on Halium 11
		* Bluetooth chip needed in-kernel workaround for a command bug
		* Forward-porting fingerprint Halium-side library



