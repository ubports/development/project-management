UBports DevSync Notes 2024-11-29

What you have been working on? What can be unblocked for you?

Guido:
- quickflux: fix qml module in Debian package
- UBsync: Debian packaging, add Save button to target editor
- dekko:
    - packaging for Debian
    - system-wide installation
- lomiri-push-service: fix more unit tests (2 remaining)

Alfred:
	* Looking into printing support (CUPS as snap, system settings + Ubuntu printing app)
		* https://github.com/ubports/ubuntu-printing-app
		* https://gitlab.com/fredldotme/lomiri-printing-app

Mike:
	* Long session with sponsor about the status of the Debian Lomiri Tablet project
	* Plans for the coming week:
		* lomiri-calendar-app -> goes mkal + Buteo
		* embedded MPRIS support in music app + mediaplayer app (still need testing on non-media-hub systems)

Raoul:
	* Nothing specific to contribute, interested to listen-in on dekko account creation (Guido's work on dekko).

Ratchanan:
	* Get to push/merge the pulseaudio secfix and start rolling out UT 20.04 OTA 7. Delayed by workflow delays at Canonical Ltd. Secfix was coordinated regarding disclosure and fix for Ubuntu.
	* Looked into media playback, fix package build for a number of packages related to media playback.
	* Cleanup GitLab labels (resulting from Github imported projects), labels consolidated at the highest group level.

Azkali:
	* Kernel of Volla X23, phone has new screen, driver adjustment needed
	* Volla Phone 22 new batch, camera change, library changes needed
		* Switch to Halium 12 port? already exists and is in use for phones that received a replacement screen
	* Aethercast work
	* qtmir work: address black screen artifacts, W-I-P

Dmitry:
	* lomiri-music-app: UI scaling not an issue with current upstream version anymore
	* lomiri-music-app: playlist index gets reset when app gets restarted [SOLVED]
	* lomiri-filemanager-app: translation problems? Tested with Russian. Looks ok.
	* lomiri-filemanager-app: file renaming problem resolved
	* Someone mentioned that the "default-open" action does not work in filemanager app, investigated + MR will come.

Neochapay:
	* Work on camera stuff. Fix qtubuntu-camera for Ubuntu noble.
	* Now checking camera application to understand what needs to be fixed there.
	* General code cleanup, amend deprecation warnings, etc.
	* one MR pushed for qtubuntu-camera

Jami:
	* Not much UBports work, a lot work on Volla Tablet. More UI work on Ubuntu Touch hopefully soon.
	* Add Volla Phone Quintus into UBports installer configs: https://github.com/ubports/installer-configs/pull/273
	* Help Azkali with debugging Volla Phone 22 new camera images


