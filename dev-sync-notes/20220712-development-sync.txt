FP 4 problems with OTA due generic port
@Alfred could you add the info

Guido:
- gallery-app, filemanager-app, notes-app, terminal-app, weather-app: port to focal, renaming, update debian packaging
- terminal-app, filemanager-app: move PamAuthentication QML module to lomiri-ui-extras
- contacted submitters of stale MRs and followed-up,  how to handle non-responsive authors:
	close feedback without feedback and not relevant
	put nice to have without feedback to board

Ubuntu webservices, should we still use them?
	geoip.ubuntu.com/lookup: might be ok if other packages are using this, follow up later
	geoname-lookup.ubuntu.com: investigate if this can be replaced by local lookups
	push.ubuntu.com: replace with UBports instance
	openweathermap API key is needed by the Debian package, need a solution
		possibly use a debconf template to input the API key on package installation, possible to preseed by vendors
		open bug report, assign to Mike
Mike >>> template for replacement 

Maik:
    Volte mannage to setup call with device specific binder via gbinder-tool.
    Lookt at ofono binder plugin and corresponing android ims ril implementation. 
    Question: What has to be implemented (only vendor specific part or radio part also the radio (standard) stuff)?
    
    
  Jami:
      Fixed freeze of System Settings on "Reset launcher" press (focal+xenial)
      Work on Volla phone 22
      
Alfred:
    Fairphone stuff
	Device configurations and overlays done wrong
		They break on OTAs
	Should work better with device specific port
	Other option: Hack system-image-server
    Performance improvent Mir android platform
	    Tuning >>Mir android platform EGL  
	Qtwebengine prevent flikkering with 
	<< what approach should be taken
	
Luca;
	testing FP4 on UT
		e.g. fingerprint has issues
		installer merge request needs to be accepted
		
Serge: 
    Worked on network and sound indicators
    
Ratchanan:
    notification in telegram for Rootfs building
    work on build/ test failings Lomiri indicators
	    debian has change in pyton-dbusmock version >> write issue for Mike on debian bug tracker
    update
    clickservice will install in all user space
    made hooks for account service to patch click
    
Milestones 
Core development
Locationd yes or no > still  keep location services?

We need to facilitate the porters now, so we can work better with the wider community.

Platform Api
	https://gitlab.com/ubports/development/core/focal-projectmanagement-missing-packages/-/issues/25#note_821972629 
	
Xenial/Focal first policy:
    for every MR we propose, we create an issue and describe the problem the MR fixes
    focal-xenial-combined MRs for repos with just a 'main' branch (easy!)
    focal first MRs on repos/components for issue fixes that can be tested in the PDK
    xenial first MRs on repos/components for issue fixes that can only be tested on real hardware
    if xenial first, then open a tracking bug / use an existing tracking bug that explains the history of MRs applied
    to xenial only and also explains why (per MR)
    if focal first, MR authors simply can file another MR against xenial (or request cherry-picking, if MR patches 
    cleanly apply)
    
