
UBports DevSync Notes 2023-12-14

What you have been working on? What can be unblocked for you?

Marius:
	* Do reviews of open MRs. Clean-up older MRs.
	* Issue with mobile data network disappearing. Probably cell tower related. Does not happen with all cell providers.
	* Start working on USB-C audio. Plan to check how it works SailfishOS.
	* Write bi-weekly report for the sponsor.
	* Request from Mike: Pull installer-configs from a ubports.com URL, hardcode that in UBports installer, announce a transition phase (e.g. 12 months) and then remove installer-configs from Github Pages.

Ratchanan:
	* Refactor ADB host verification part out of ayatana-indicator-display and move it into its own upstream project called adbd-approver. ADBd code in ayatana-indicator-display is very solid and self-contained.
	* As Marius just merged the code for adbd-approver, the ADB host verification will be available and functional, but could cause some pains to porters.
	* Review MRs.
	* Question: ADB approver inherits strings from ayatana-indicator-display, where shall we place it on hosted.weblate.org -> Lomiri group
	* Starting next Monday, "brutal" archiving of Github project will start...

Jami:
	* Looking into wireless display / aethercast. On X23 it now displays the image for about 3sec then it vanishes.
	* Volla 22: Halium 12 rootfs image (20.04/arm64/android9plus/staging) not booting while CI built one does, needs investigation still
	* Volla 22 installer config update, request for merging https://github.com/ubports/installer-configs/pull/238

Alfred:
	* opticd stuff: service that bridges the camera over to v4l2-loopback, dkms module for desktop Linux
		* opticd purpose (Raoul asks): Many Linux applications know to talk to the kernel using /dev/videoX; they don't have any idea about Android's hardware abstraction layer.
		* opticd with v4l2-loopback module will provide a virtual video device where normal Linux applications can access the webcam (where opticd pulls camera frames OpenGL-based from the Camera HAL to /dev/videoX)
	* SIP calling: together with Lionel, PoC video "published", currently a race condition in the code, maybe (because in debug mode everything works, but not without).
		* Hints from Ratchanan: gstreamer, track element ownership; Alfred: gstreamer pipelines probably ok, conference plugin ("farstream") emits a few events that seemingly come in out of order.
		* Ratchanan: <second aspect, not fully remembered>

Raoul:
	* Question: JB is highly interested in getting Lomiri running on a desktop. Login problems, short debugging session. To be continued tomorrow.

Mike:
	* Meet with Marius and Ratchanan about initial bug submission entry point, transfer github.com/ubports/ubuntu-touch to gitlab.com/ubports/ubuntu-touch.
	* Bug triaging session on Friday (tomorrow)

General discussion:
	* About milestone planning.
	* When will 24.4 development start? Beginning of next year, once Canonical Ltd. stabilizes their work on current development branches.
	* February 29th Ubuntu next LTS freeze
	* Wait after 21st Dec 2023, then we can start building 24.04 based packages




