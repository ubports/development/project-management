UBports DevSync Notes 2024-02-22

What you have been working on? What can be unblocked for you?

Ratchanan:
	* Working on infrastructure, code for supporting new branching scheme.
		* CI build-tool scripts, migrate from Github to GitLab
		* Adjust build-tools, so that we can build against our Ubuntu release and Debian testing
		* gitlab auto backporter (fresh out of the oven)
	* Starting from today on: work on bootstrapping Ubuntu noble packages of Ubuntu Touch

Alfred:
	* Discuss adaptation of UBports logo to new Ubuntu branding
	* Permission by Canonical in general, will offer various community artwork for them to rule out "too similar" logos
	* Community artwork
	* Requirement: Not to look too similar to Canonical products

Lionel:
	* Still  "playing" with nemo-qml-plugin-contact on address-book-app which would allows better support for contact sorting, alphabetical indices, search etc... 
	* Keep studying the new contact backend, here are some writings: https://gitlab.com/ubports/development/project-management/-/wikis/New-Backend-for-Address-Book

Raoul:
	* Install Debian Lomiri on Lenovo Yoga, install Ubuntu with Lomiri to the same device

Mike
	* Get Maliit Keyboard working in Lomiri on Debian.
	* Get lomiri-greeter working with LightDM.
	* Debian uploads of all achievements in Lomiri for Debian.

Guido
	* Lomiri Debian desktop integration
		* looked into unifying Debian and Ubuntu Touch session scripts
		* possibly streamline and replace by systemd services/targets, use of systemd dependency mechanisms
	* debugged maliit-keyboard in qemu VMs w/ attached keyboard and Plasma desktop and Lomiri
		* debugged override to always show OSK in Lomiri
		* maliit itself does not show unless there is no physical keyboard attached and a touch focus event on an input widget
