Development sync for March 08, 2022, 1430 UTC


Agenda

	* What have you been working on this week? 
		* Save suggestions until after the meeting is over, please
	* Are you having trouble with anything?

	* Discuss roadmap what are the scenario's for the coming months this year

	* When's our next sync?

Minutes


Ratchanan (limited amount of hours)
	* USB tethering (usbmoded)
	* Ayatana Indicators renewed

Mike
	* Lomiri UI extra's 0.5
		* Upstream release
		* Package uploaded to Debian
	* maliit-framework and maliit-keyboard packaged for Debian (Rodney needs to implement it in UT)
	* Click (doesn't build as root user) needs to be checked >  more time
	* MR/PR reviews and feedback given (Ayatana Indicators, Lomiri)
  
Guido
	* libxkbcommon: backport from jammy
	* locationd: finished package for focal, one failing test (GPS)
	* qtubuntu-camera: package for focal, debugging
	* documentation: reorganized, corrected docs on overlay files
	* qtorganizer5-eds: rereview, debug failing tests

Marius
	* Debian finnished Lomiri system-settings, dbus-cpp, net-cpp, geonames
	* locationd refactoring, split up big commit into individual commits (still W-I-P)
	* platform-api: client-side things have been moved over to locationd
	* lomiri patches about locationd/platform-api, remove Android dependencies, use deviceinfo
	* Trying to run droidian and run Lomiri on it 


Include waydroid in core apps? (NoKit)

Mike
	* KDE Qt communicatrion
	* Review debian (marius) packages + formalistic stuff

Marius
	* Blocking: Mike has look at Lomiri packaging (copyright)

Ratchanan
	* Blocking: ayatana-indicator > epoc < remove package from repo, rebuild and add it back (Florian acces for repo)
	* Blocking: [Mike] look at usb-moded

Guido
	* Blocking: Telaphatie Ofono needs 
	* Camera app needs to be chaneged from master to main
	* Ubuntucamera api's  renamed
	* Blocking: Camera app migrated to Gitlab [Ratchanan]
	
	
Guido
Guido says:
https://ci.ubports.com/job/docs.ubports.com/job/PR-492/lastSuccessfulBuild/artifact/_build/html/porting/configure_test_fix/Overlay.html
 
15:52
Ratchanan
Ratchanan says:
https://gitlab.com/ubports/core/packaging/ayatana-indicators/ayatana-indicator-messages/-/commit/4e6bb52951c4212073b98d2639f40a5e8ce74b09
 
Ratchanan says:
https://gitlab.com/ubports/core/packaging/usb-moded/-/merge_requests/1
 


